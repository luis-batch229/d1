// Default setup of Express
// "require" to load the module/package 

const e = require("express");
const express = require("express");

// Creation of an app/project
// In layman's terms, app is our server
const app = express();
const port = 3000;

// Setup for allowing the server to handle data from request
// middlwares


app.use(express.json()); // allowing system to handle API

// allows your app to read data from forms
app.use(express.urlencoded({ extended: true }));

// [SECTION] - Routes 

// GET Method
app.get("/", (req, res) => {
  res.send("hello asis");
});

// POST Method
app.post("/hello", (req, res) => {
  res.send(`Hello There ${req.body.firstname} ${req.body.lastname}!`);
});

let users = [];

app.post("/signup", (req, res) => {
  console.log(req.body);
  // if the username and password is not empty the user info will be pushed/stored to our mock database
  if (req.body.username !== "" && req.body.password !== "") {
    users.push(req.body);
    res.send(`user ${req.body.username} successfully registered!`)
  } else {
    res.send("Pls input BOTH username and password");
  }
});

// PUT/UPDATE METHOD
app.put("/change-password", (req, res) => {
  // creates a var to store message to be sent back to the client
  let message;

  // Creates a for loop that will loop through the elements of "users" array
  for (let i = 0; i < users.length; i++) {
    //  If the username provided in the client/postman and the username of the current object is the same
    if (req.body.username == users[i].username) {
      // Changes the password of the user found by the loop
      users[i].password = req.body.password;

      message = `User ${req.body.username}'s 
      password has been updated.`;

      break;
    } else {
      // if no user found
      message = "user does not exist po.";
    }

  }
  res.send(message);


});

// CODE ALONG ACTIVITY

// Home page
app.get("/home", (req, res) => {
  res.send("welcome to homepage!");
});
// USERS
app.get("/users", (req, res) => {
  res.send(users);
});

// DELETE USER
app.delete("/delete-user", (req, res) => {
  let message;

  if (users.length != 0) {
    for (let i = 0; i < users.length; i++) {
      if (req.body.username = users[i].username) {
        users.splice(users[i], 1);
        message = `User ${req.body.username} has been deleted.`
        break;
      } else {
        res.send(users)
      }
    }
  } else {
    message = "users is empty";
  }
  res.send(message);
});










app.listen(port, () => console.log(`Server is running at port ${port}.`));

